package net.pl3x.bukkit.alliance;

import net.pl3x.bukkit.alliance.command.CmdAlliance;
import net.pl3x.bukkit.alliance.configuration.Config;
import net.pl3x.bukkit.alliance.configuration.Lang;
import net.pl3x.bukkit.alliance.listener.BukkitListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class AllianceCore extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            Logger.error("# GriefPrevention NOT found and/or enabled!");
            Logger.error("# This plugin requires GriefPrevention to be installed and enabled!");
            return;
        }

        getServer().getPluginManager().registerEvents(new BukkitListener(), this);

        getCommand("alliance").setExecutor(new CmdAlliance(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static AllianceCore getPlugin() {
        return AllianceCore.getPlugin(AllianceCore.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }
}
