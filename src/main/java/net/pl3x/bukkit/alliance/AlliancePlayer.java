package net.pl3x.bukkit.alliance;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AlliancePlayer {
    private final UUID uuid;

    private Alliance alliance;

    private Rank rank;

    private long policyPoints;
    private long accumulatedPolicyPoints;

    private final Map<Alliance, Long> contributionPoints = new HashMap<>();
    private final Map<Alliance, Long> accumulatedContributionPoints = new HashMap<>();

    private final Map<Alliance, Integer> kills = new HashMap<>();
    private final Map<Alliance, Integer> deaths = new HashMap<>();
    private final Map<Alliance, Integer> assists = new HashMap<>();

    private final Map<LocalDateTime, String> recentActivity = new HashMap<>();

    public AlliancePlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public AlliancePlayer(Player player) {
        this(player.getUniqueId());
    }

    public UUID getUUID() {
        return this.uuid;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(getUUID());
    }
}
