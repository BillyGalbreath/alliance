package net.pl3x.bukkit.alliance;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public enum Alliance {
    ENLIGHTENED_UNION("Enlightened Union", TeamColor.GREEN),
    FREE_TRADERS("Free Traders Collective", TeamColor.YELLOW),
    IRON_ORDER("Iron Order", TeamColor.BLUE);

    private final String name;
    private final TeamColor color;

    private final Set<UUID> members = new HashSet<>();

    Alliance(String name, TeamColor color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public TeamColor getColor() {
        return color;
    }
}
