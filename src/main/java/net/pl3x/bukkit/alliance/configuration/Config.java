package net.pl3x.bukkit.alliance.configuration;

import net.pl3x.bukkit.alliance.AllianceCore;
import net.pl3x.bukkit.alliance.Logger;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static final Map<String, Integer> INCREMENTS = new HashMap<>();
    public static final Map<String, Integer> MAXCAPS = new HashMap<>();

    public static void reload() {
        AllianceCore plugin = AllianceCore.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");

        for (String key : config.getConfigurationSection("accruals.increment").getKeys(false)) {
            int value = config.getInt("accruals.increment." + key, -1);
            if (value < 0) {
                Logger.warn("Increment value for '" + key + "' is invalid. Ignoring.");
                continue;
            }
            INCREMENTS.put(key, value);
        }

        for (String key : config.getConfigurationSection("accruals.maxcap").getKeys(false)) {
            int value = config.getInt("accruals.maxcap." + key, -1);
            if (value < 0) {
                Logger.warn("Max cap value for '" + key + "' is invalid. Ignoring.");
                continue;
            }
            MAXCAPS.put(key, value);
        }
    }
}
