package net.pl3x.bukkit.alliance;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Color;

public enum TeamColor {
    BLUE(Color.BLUE, ChatColor.DARK_BLUE),
    GREEN(Color.GREEN, ChatColor.GREEN),
    YELLOW(Color.YELLOW, ChatColor.YELLOW);

    private final Color color;
    private final ChatColor chatColor;

    TeamColor(Color color, ChatColor chatColor) {
        this.color = color;
        this.chatColor = chatColor;
    }

    public Color getColor() {
        return color;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }
}
