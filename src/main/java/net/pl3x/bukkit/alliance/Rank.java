package net.pl3x.bukkit.alliance;

public enum Rank {
    EXARCH,
    RAID_COMMANDER,
    SECURITY_COMMANDER,
    SUPPLY_COMMANDER,
    ECHELON_1,
    ECHELON_2,
    ECHELON_3,
    ECHELON_4,
    MEMBER,
    CADET
}
